import React, { Component } from 'react';
import './Footer.css';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }

    render() {
        return (
            <React.Fragment>
                <footer className="text-center text-secondary shadow bg-white p-4">
                    <p className="mb-0">Copyright © 2021 Corporate Electric | All Rights Reserved.</p>
                </footer>
            </React.Fragment>
        );
    }
}

export default Header;