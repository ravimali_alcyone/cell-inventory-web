import React, { Component } from 'react';
import './Card.css';

class CopyCard extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }

    render() {
        return (
            <React.Fragment>
                <div className="dashCard copyCard" onClick={this.props.handleEvent}>
                    <h3 className="card_number w-100">
                        <mark>{this.props.number}</mark>
                        <span className="float-right">${this.props.cost}</span>
                    </h3>
                    <h5 className="card_title" style={{marginTop: '6px'}}>{this.props.title}</h5>
                </div>
            </React.Fragment>
        );
    }
}

export default CopyCard;