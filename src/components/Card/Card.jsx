import React, { Component } from 'react';
import { Link } from "react-router-dom";
import './Card.css';

class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }

    render() {
        return (
            <React.Fragment>
                <Link className="dashCard" to={this.props.routeName} onClick={this.props.handleEvent}>
                    <h5 className="card_title">{this.props.title}</h5>
                </Link>
            </React.Fragment>
        );
    }
}

export default Card;