import React, { Component } from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";
import logo from './../../assets/images/logo.jpg';
import './Header.css';

class Header extends Component {
    constructor(props) {
        const userData = JSON.parse(localStorage.getItem('userInfo'));

        super(props);
        this.state = {
            userData: userData
        };
    }

    signOut = () => {
        localStorage.clear();
        window.location.href = '/login';
    }

    render() {
        return (
            <React.Fragment>
                <header className="bg-white shadow-sm pt-3 pb-3">
                    <nav className="navbar navbar-expand-lg navbar-dark">
                        <div className="logo">
                            <Link to="dashboard"><img className="img-fluid" src={logo} alt="Hoffman Helping Hands" /></Link>
                        </div>
                        <button className="navbar-toggler ml-auto p-0" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="icon-bar top-bar"></span>
                            <span className="icon-bar middle-bar"></span>
                            <span className="icon-bar bottom-bar"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarResponsive">
                            <ul className="nav navbar-nav ml-auto">
                                <li className="nav-item">
                                    <div className="dropdown top_rght_head ml-auto">
                                        <Router>
                                            <Link to="#" className="nav-link dropdown-toggle text-secondary font-weight-bold" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">Welcome, {this.state.userData.fullName} <b className="caret"></b></Link>
                                            <div className="dropdown-menu dropdown-menu-right position-absolute" aria-labelledby="navbarDropdown">
                                                <Link to="/login" className="dropdown-item font-weight-bold" onClick={this.signOut}>Sign out</Link>
                                            </div>
                                        </Router>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>
            </React.Fragment>
        );
    }
}

export default Header;