import React, { Component } from 'react';
import loader from './../../assets/images/loader.svg';
import './Loader.css';

class Loader extends Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }

    render() {
        return (
            <React.Fragment>
                <div className="loaderWrapper">
                    <img src={loader} alt="Loader"/>
                </div>
            </React.Fragment>
        );
    }
}

export default Loader;