import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({children, ...rest}) => {
    var isAuth;
    var params = {...rest};
    let userInfo = JSON.parse(localStorage.getItem('userInfo'));
    let isLoggedIn = parseInt(localStorage.getItem('isLoggedIn'));

    if (userInfo !== null) {
        var recievedDate = new Date(userInfo.expTime);
        var nowDate   = new Date();
        if (Date.parse(nowDate) > Date.parse(recievedDate)) {
            localStorage.clear();
            return (<Redirect to="/login" />);
        }
    }

    if (params.path === '/allUsers' || params.path === '/createCategory' || params.path === '/updateCategory' || params.path === '/uploadExcel' || params.path === '/manageCategories') {
        if (userInfo !== null && userInfo.isAdmin === true) {
            return (<Route {...rest} render={ () => children } />);
        } else {
            if (userInfo !== null && isLoggedIn === 1) {
                return (<Redirect to="/dashboard" />);
            } else {
                return (<Redirect to="/login" />);
            }
        }
    }

    if (userInfo !== null && isLoggedIn === 1) {
        isAuth = true;
    } else {
        isAuth = false;
    }
    return (<Route {...rest} render={ () => isAuth ? (children) : (<Redirect to="/login" />)} />)
}

export default PrivateRoute;