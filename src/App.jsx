import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import Login from './containers/Login/Login';
import ForgotPassword from './containers/ForgotPassword/ForgotPassword';
import ResetPassword from './containers/ResetPassword/ResetPassword';
import Signup from './containers/Signup/Signup';
import Dashboard from './containers/Dashboard/Dashboard';
import PrivateRoute from './PrivateRoutes';
import Users from './containers/Users/Users';
import UploadExcel from './containers/UploadExcel/UploadExcel';
import FilterInfo from './containers/FilterInfo/FilterInfo';
import Search from './containers/Search/Search';

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
	return (
		<div className="App">
			<Router>
				<Switch>
					<Route exact path="/" component={Login} />
					<Route path="/login" component={Login} />
					<Route path="/forgotPwd" component={ForgotPassword} />
					<Route path="/signup" component={Signup} />
					<Route path="/resetPwd" component={ResetPassword} />
					<PrivateRoute exact path="/dashboard" loggedIn="1">
						<Dashboard />
					</PrivateRoute>
					<PrivateRoute exact path="/allUsers" loggedIn="1">
						<Users />
					</PrivateRoute>
					<PrivateRoute exact path="/uploadExcel" loggedIn="1">
						<UploadExcel />
					</PrivateRoute>
					<PrivateRoute exact path="/filterInfo" loggedIn="1">
						<FilterInfo />
					</PrivateRoute>
					<PrivateRoute exact path="/search" loggedIn="1">
						<Search />
					</PrivateRoute>
				</Switch>
			</Router>
		</div>
	);
}

export default App;
