import React, { Component } from 'react';
import { Link } from "react-router-dom";
import config from './../../config';
import axios from 'axios';
import { toast } from 'react-toastify';
import { Formik } from 'formik';
import 'react-toastify/dist/ReactToastify.css';
import './ResetPassword.css';

class ResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            baseURI: config.baseURI,
            reqCompleted: false,
            otp: '',
            password: ''
        };
        toast.configure();
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = (value) => {
        this.setState({ reqCompleted: true });
        let data = {
            email: localStorage.getItem('userEmail'),
            otp_code: value.otp,
            password: value.password
        };

        axios({
            url: `${this.state.baseURI}users/reset-password`,
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=UTF-8'
            },
            data: data
        }).then(response => {
            this.setState({ reqCompleted: false });
            if (response.data.status) {
                toast.success(response.data.message);
                this.props.history.push('/login');
            } else {
                toast.error(response.data.message);
            }
        }).catch(error => {
            toast.error(error.response.data.message);
            this.setState({ reqCompleted: false });
        });
    }

    render() {
        return (
            <React.Fragment>
                <section className="page_wrap">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="cp_login_box border rounded-lg p-4 bg-light mx-auto">
                                    <div className="title_box text-center">
                                        <h1 className="primary_title">Reset Password</h1>
                                    </div>
                                    <Formik
                                        initialValues={{ otp: '', password: '' }}
                                        validate={values => {
                                            const errors = {};

                                            if (!values.otp) {
                                                errors.otp = 'OTP is required.';
                                            } else if (values.otp.length < 4) {
                                                errors.otp = 'OTP should be 4 digits.';
                                            } else {
                                                var pattern = new RegExp(/^[0-9\b]+$/);
                                                if (!pattern.test(values.otp)) {
                                                    errors.otp = 'Please enter only number.';
                                                }
                                            }

                                            if (!values.password) {
                                                errors.password = 'Password is required.';
                                            }

                                            return errors;
                                        }}
                                        onSubmit={(values, { setSubmitting }) => {
                                            this.handleSubmit(values);
                                            setTimeout(() => {
                                                setSubmitting(false);
                                            }, 400);
                                        }}
                                    >
                                        {({
                                            values,
                                            errors,
                                            touched,
                                            handleChange,
                                            handleBlur,
                                            handleSubmit,
                                            isSubmitting
                                        }) => (
                                            <form className="mt-5" onSubmit={handleSubmit}>
                                                <div className="form-group text-left">
                                                    <label>OTP<sup className="text-danger">*</sup></label>
                                                    <input type="text" name="otp" value={values.otp} onChange={handleChange} onBlur={handleBlur} className="form-control" placeholder="Enter OTP" maxLength="4" />
                                                    <small className="text-danger">{errors.otp && touched.otp && errors.otp}</small>
                                                </div>
                                                <div className="form-group text-left">
                                                    <label>New Password<sup className="text-danger">*</sup></label>
                                                    <input type="password" name="password" value={values.password} onChange={handleChange} onBlur={handleBlur} className="form-control" placeholder="New Password" />
                                                    <small className="text-danger">{errors.password && touched.password && errors.password}</small>
                                                </div>
                                                <div className="from_btm_btns_grp mb-0 d-flex justify-content-between">
                                                    <button type="button" className="btn btn-primary btn_ani" id="btnBackCpLogin"><Link to="/" className="text-white text-decoration-none">Back to User Login</Link></button>
                                                    <button type="submit" className={"btn btn-primary btn_ani w-25 float-right" + (this.state.reqCompleted ? ' cursor_no_drop' : '')} id="btnPwdRecovery" disabled={isSubmitting}>{!this.state.reqCompleted || <span className="spinner-border spinner-border-sm mr-1"></span> } Submit</button>
                                                </div>
                                            </form>
                                        )}
                                    </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default ResetPassword;