import React, { Component } from 'react';
import Header from './../../components/Header/Header';
import Footer from './../../components/Footer/Footer';
import Loader from './../../components/Loader/Loader';
import axios from 'axios';
import config from './../../config';
import { toast } from 'react-toastify';
import Switch from "react-switch";
import { Link } from "react-router-dom";
import 'react-toastify/dist/ReactToastify.css';
import './Users.css';
import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
import $ from 'jquery';

class Users extends Component {
    constructor(props) {
        const userInfo = JSON.parse(localStorage.getItem('userInfo'));
        super(props);
        this.state = {
            userInfo: userInfo,
            baseURI: config.baseURI,
            usersData: '',
            checked: false,
            loading: false
        };
        toast.configure();
    }

    componentDidMount() {
        axios({
            url: `${this.state.baseURI}users`,
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=UTF-8',
                'Authorization': 'Bearer ' + this.state.userInfo.token
            }
        }).then(response => {
            this.setState({usersData: response.data.data});
            setTimeout(() => {
                $('#usersTable').DataTable({
                    "scrollY": "800px",
                    "scrollCollapse": true,
                });
            }, 100);
        }).catch(error => {
            toast.error(error.response.data.message);
        });
    }

    handleChange(status, id)  {
        this.setState({loading: true});

        var usersData = [...this.state.usersData];
        var index = usersData.findIndex(obj => obj.id === id);
        if (usersData[index].isActive === true) {
            usersData[index].isActive = false;
        } else {
            usersData[index].isActive = true;
        }

        setTimeout(() => {
            axios({
                url: `${this.state.baseURI}users/status-update/${id}`,
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                    'Authorization': 'Bearer ' + this.state.userInfo.token
                },
                data: {is_active: status === true ? false : true}
            }).then(response => {
                this.setState({loading: false});
                if (response.data.status) {
                    this.setState({usersData});
                    toast.success(response.data.message);
                } else {
                    toast.error(response.data.message);
                }
            }).catch(error => {
                this.setState({loading: false});
                toast.error(error.response.data.message);
            });
        }, 1000);
    }

    render() {
        return (
            <React.Fragment>
                <Header />
                    <div className="breadcrumb text-white mb-0">
                        <Link to='/dashboard' className="text-white">Dashboard</Link> <span>&nbsp; /</span>
                        <Link to='/allUsers' className="text-white">&nbsp; Users</Link>
                    </div>

                    <section className="fullPageWrap content_wrapper users_listing position-relative pt-4 pb-4">
                        {!this.state.loading || <Loader />}

                        <div className="container">
                            <div className="row">
                                <div className="col">
                                    <div className="title_box text-center">
                                        <h1 className="primary_title center">Users</h1>
                                    </div>

                                    <div className="card p-3 border rounded bg-white w-100 mt-5">
                                        <table id="usersTable" className="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Full Name</th>
                                                    <th scope="col">Email</th>
                                                    <th scope="col">Phone</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {Object.keys(this.state.usersData).map((user, i) => (
                                                    Object.keys(user).map(key=>(
                                                        <tr key={key}>
                                                            <th scope="row">{this.state.usersData[user].fullName}</th>
                                                            <td>{this.state.usersData[user].email}</td>
                                                            <td>{this.state.usersData[user].phone}</td>
                                                            <td>
                                                                <label>
                                                                    <Switch onChange={isActive => this.handleChange(this.state.usersData[user].isActive, this.state.usersData[user].id)} checked={this.state.usersData[user].isActive} id={this.state.usersData[user].id} checkedIcon={false} uncheckedIcon={false} />
                                                                </label>
                                                            </td>
                                                        </tr>
                                                    )
                                                )))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                <Footer />
            </React.Fragment>
        );
    }
}

export default Users;