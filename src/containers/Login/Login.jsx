import React, { Component } from 'react';
import { Link } from "react-router-dom";
import config from './../../config';
import axios from 'axios';
import { toast } from 'react-toastify';
import { Formik } from 'formik';
import 'react-toastify/dist/ReactToastify.css';
import './Login.css';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            baseURI: config.baseURI,
            reqCompleted: false,
            email: '',
            password: ''
        };
        toast.configure();
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = (value) => {
        this.setState({ reqCompleted: true });
        let data = {
            email: value.email,
            password: value.password
        };

        axios({
            url: `${this.state.baseURI}users/authenticate`,
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=UTF-8'
            },
            data: data
        }).then(response => {
            this.setState({ reqCompleted: false });
            if (response.data.status) {
                toast.success(response.data.message);
                localStorage.setItem('userInfo', JSON.stringify(response.data.data));
                localStorage.setItem('isLoggedIn', 1);
                this.props.history.push('/dashboard');
            } else {
                toast.error(response.data.message);
            }
        }).catch(error => {
            toast.error(error.response.data.message);
            this.setState({ reqCompleted: false });
        });
    }

    render() {
        return (
            <React.Fragment>
                <section className="page_wrap login">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="cp_login_box border rounded-lg p-4 bg-light mx-auto">
                                    <div className="title_box text-center">
                                        <h1 className="primary_title">Login</h1>
                                    </div>
                                    <Formik
                                        initialValues={{ email: '', password: '' }}
                                        validate={values => {
                                            const errors = {};
                                            if (!values.email) {
                                                errors.email = 'Email is required.';
                                            } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
                                                errors.email = 'Invalid email address';
                                            }

                                            if (!values.password) {
                                                errors.password = 'Password is required.';
                                            }
                                            return errors;
                                        }}
                                        onSubmit={(values, { setSubmitting }) => {
                                            this.handleSubmit(values);
                                            setTimeout(() => {
                                                setSubmitting(false);
                                            }, 400);
                                        }}
                                    >
                                        {({
                                            values,
                                            errors,
                                            touched,
                                            handleChange,
                                            handleBlur,
                                            handleSubmit,
                                            isSubmitting
                                        }) => (
                                            <form className="mt-5" onSubmit={handleSubmit}>
                                                <div className="form-group text-left">
                                                    <label>Email<sup className="text-danger">*</sup></label>
                                                    <input type="email" name="email" value={values.email} onChange={handleChange} onBlur={handleBlur} className="form-control" placeholder="Please enter email" />
                                                    <small className="text-danger">{errors.email && touched.email && errors.email}</small>
                                                </div>
                                                <div className="form-group text-left">
                                                    <label>Password<sup className="text-danger">*</sup></label>
                                                    <input type="password" name="password" value={values.password} onChange={handleChange} onBlur={handleBlur} className="form-control" placeholder="Please enter password" />
                                                    <small className="text-danger">{errors.password && touched.password && errors.password}</small>
                                                </div>
                                                <div className="from_btm_btns_grp">
                                                    <button type="submit" className={"btn btn-primary btn_ani mx-auto mb-3 w-100" + (this.state.reqCompleted ? ' cursor_no_drop' : '')} id="btnCpLogin" disabled={isSubmitting}>{!this.state.reqCompleted || <span className="spinner-border spinner-border-sm mr-1"></span> } Login</button>
                                                </div>
                                                <div className="forget_links text-left">
                                                    <Link to="/forgotPwd" className="text-left">Forgot Password?</Link>
                                                    <Link to="/signup" className="text-left float-right">Create Account?</Link>
                                                </div>
                                            </form>
                                        )}
                                    </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default Login;