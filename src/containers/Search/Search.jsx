import React, { Component } from 'react';
import Header from './../../components/Header/Header';
import Footer from './../../components/Footer/Footer';
import CopyCard from './../../components/Card/CopyCard';
import { Link } from "react-router-dom";
import axios from 'axios';
import config from './../../config';
import { toast } from 'react-toastify';
import Autosuggest from 'react-autosuggest';
import 'react-toastify/dist/ReactToastify.css';
import './Search.css';

class Search extends Component {
    constructor(props) {
        const userInfo = JSON.parse(localStorage.getItem('userInfo'));

        super(props);
        this.state = {
            userInfo: userInfo,
            baseURI: config.baseURI,
            filteredData: [],
            dataNotFound: false,
            isCopied: false,
            value: '',
            suggestions: [],
            inventoryData: []
        };

        toast.configure();
    }

    // Teach Autosuggest how to calculate suggestions for any given input value.
    getSuggestions = value => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
        // return inputLength === 0 ? [] : this.state.inventoryData.filter(inventory =>
        //     inventory.trade_description !== undefined ? inventory.trade_description.toLowerCase().slice(0, inputLength) === inputValue : ''
        // );
        return inputLength === 0 ? [] : this.state.inventoryData.map(inventory => inventory);
    };

    // When suggestion is clicked, Autosuggest needs to populate the input
    // based on the clicked suggestion. Teach Autosuggest how to calculate the
    // input value for every given suggestion.
    getSuggestionValue = suggestion => suggestion.trade_description;

    // Use your imagination to render suggestions.
    renderSuggestion = suggestion => (
        <div>{suggestion.trade_description}</div>
    );

    copy = (text) => event => {
        if (navigator.clipboard && window.isSecureContext) {
            navigator.clipboard.writeText(text);
            this.setState({isCopied: true});
            setTimeout(() => {
                this.setState({isCopied: false});
            }, 2000);
        } else {
            let textArea = document.createElement("textarea");
            textArea.value = text;
            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();
            return new Promise((res, rej) => {
                document.execCommand('copy') ? res() : rej();
                textArea.remove();
                this.setState({isCopied: true});
                setTimeout(() => {
                    this.setState({isCopied: false});
                }, 2000);
            });
        }
    }

    // Suggessions
    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue
        });

        axios({
            url: `${this.state.baseURI}filters/get-search-data`,
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=UTF-8',
                'Authorization': 'Bearer ' + this.state.userInfo.token
            },
            data: {search_value: newValue}
        }).then(response => {
            if (response) {
                this.setState({inventoryData: response.data.data, dataNotFound: false});
            }
        }).catch(error => {
            // toast.error(error.response.data.message);
            this.setState({filteredData: [], dataNotFound: true});
        });
    };

    // Autosuggest will call this function every time you need to update suggestions.
    // You already implemented this logic above, so just use it.
    onSuggestionsFetchRequested = ({ value }) => {
        setTimeout(() => {
            this.setState({
                suggestions: this.getSuggestions(value)
            });
        }, 200);
    };

    // Autosuggest will call this function every time you need to clear suggestions.
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    onSuggestionSelected = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
        this.setState({filteredData: [suggestion]});
    }

    render() {
        const { value, suggestions } = this.state;

        // Autosuggest will pass through all these props to the input.
        const inputProps = {
            placeholder: 'Search...',
            value,
            onChange: this.onChange
        };

        return (
            <React.Fragment>
                <Header />
                <div className="breadcrumb text-white mb-0">
                    <Link to='/dashboard' className="text-white">Dashboard</Link> <span>&nbsp; /</span>
                    <Link to='/uploadExcel' className="text-white">&nbsp; Search</Link>
                </div>

                <section className="fullPageWrap cardsSec">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="pageWrap bg-white shadow-sm rounded-lg">
                                    <div className="process_status_bar search w-100">
                                        <Autosuggest
                                            suggestions={suggestions}
                                            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                            getSuggestionValue={this.getSuggestionValue}
                                            renderSuggestion={this.renderSuggestion}
                                            inputProps={inputProps}
                                            onSuggestionSelected={this.onSuggestionSelected}
                                        />
                                    </div>
                                    <div className="title_area search">
                                        <span className={this.state.isCopied?'copiedMsg fadeIn':'copiedMsg fadeOut'}>Copied!</span>
                                    </div>
                                    <div className="card_btn_wrap">
                                        {Object.keys(this.state.filteredData).map(key => (
                                            <CopyCard key={key} number={this.state.filteredData[key].inventory_part_number} title={this.state.filteredData[key].trade_description} cost={this.state.filteredData ? parseFloat(this.state.filteredData[key].cost_usd).toFixed(2) : ''} handleEvent={this.copy(this.state.filteredData[key].inventory_part_number)} />
                                        ))}
                                        {!this.state.dataNotFound || <p className="text-center text-secondary w-100">Data Not Found.</p>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <Footer />
            </React.Fragment>
        );
    }
}

export default Search;