import React, { Component } from 'react';
import Header from './../../components/Header/Header';
import Footer from './../../components/Footer/Footer';
import Card from './../../components/Card/Card';
import axios from 'axios';
import config from './../../config';
import { toast } from 'react-toastify';
import { Link } from "react-router-dom";
import { confirmAlert } from 'react-confirm-alert';
import './Dashboard.css';
import 'react-toastify/dist/ReactToastify.css';
import 'react-confirm-alert/src/react-confirm-alert.css';

class Dashboard extends Component {
    constructor(props) {
        const userInfo = JSON.parse(localStorage.getItem('userInfo'));

        super(props);
        this.state = {
            userInfo: userInfo,
            baseURI: config.baseURI,
            categories: ''
        };
    }

    componentDidMount() {
        axios({
            url: `${this.state.baseURI}filters/get-categories`,
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=UTF-8',
                'Authorization': 'Bearer ' + this.state.userInfo.token
            }
        }).then(response => {
            if (response.data.status) {
                this.setState({categories: response.data.data});
            }
        }).catch(error => {
            if (error.response !== undefined) {
                toast.error(error.response.data.message);
            }
        });
    }

    handleCategory = category => event => {
        localStorage.setItem('selectedCategory', JSON.stringify(category));
    }

    clearCategoriesData = () => {
        axios({
            url: `${this.state.baseURI}admin/categories/cleardata`,
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=UTF-8',
                'Authorization': 'Bearer ' + this.state.userInfo.token
            }
        }).then(response => {
            if (response.data.status) {
                toast.success(response.data.message);
                this.setState({categories: ''});
            }
        }).catch(error => {
            toast.error(error.response.data.message);
        });
    }

    confirmation = () => {
        confirmAlert({
          title: 'Confirm to Clear',
          message: 'Are you sure you would like to clear all data from the database?',
          buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.clearCategoriesData()
                },
                {
                    label: 'No',
                    onClick: () => ''
                }
            ]
        });
    };

    render() {
        return (
            <React.Fragment>
                <Header />
                <div className="breadcrumb text-white mb-0">
                    <Link to='/dashboard' className="text-white">Dashboard</Link>
                </div>

                <div className="fullPageWrap">
                    { this.state.userInfo.isAdmin ?
                        <section className="cardsSec">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="pageWrap bg-white shadow-sm rounded-lg">
                                            <div className="title_area">
                                                <h2 className="dash_title">Settings</h2>
                                                <button className="btn btn-primary" onClick={this.confirmation}>Clear Data</button>
                                            </div>
                                            <div className="card_btn_wrap w-100">
                                                <Card title="Manage Users" routeName="/allUsers" />
                                                {/* <Card title="Manage Categories" routeName="/manageCategories" /> */}
                                                <Card title="Upload Excel" routeName="/uploadExcel" />
                                                <Card title="Search" routeName="/search" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section> :
                        <section className="cardsSec">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="pageWrap bg-white shadow-sm rounded-lg">
                                            <div className="title_area">
                                                <h2 className="dash_title">Settings</h2>
                                            </div>
                                            <div className="card_btn_wrap w-100">
                                                <Card title="Search" routeName="/search" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    }

                    <section className="cardsSec">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="pageWrap bg-white shadow-sm rounded-lg">
                                        <div className="title_area">
                                            <h2 className="dash_title">Categories</h2>
                                        </div>
                                        <div className="card_btn_wrap">
                                            {Object.keys(this.state.categories).map((category, i) => (
                                                this.state.categories[category] !== null && this.state.categories[category] !== ''?
                                                    <Card
                                                        title={this.state.categories[category]}
                                                        routeName="/filterInfo"
                                                        key={category}
                                                        // category={this.state.categories[category]}
                                                        handleEvent={this.handleCategory(this.state.categories[category])}
                                                    />
                                                : ''
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <Footer />
            </React.Fragment>
        );
    }
}

export default Dashboard;