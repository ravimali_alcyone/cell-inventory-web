import React, { Component } from 'react';
import Header from './../../components/Header/Header';
import Footer from './../../components/Footer/Footer';
import Card from './../../components/Card/Card';
import CopyCard from './../../components/Card/CopyCard';
import { Link } from "react-router-dom";
import axios from 'axios';
import config from './../../config';
import { toast } from 'react-toastify';
import './FilterInfo.css';
import 'react-toastify/dist/ReactToastify.css';

class FilterInfo extends Component {
    constructor(props) {
        const userInfo = JSON.parse(localStorage.getItem('userInfo'));
        const selectedCategory = JSON.parse(localStorage.getItem('selectedCategory'));

        super(props);
        this.flag = 0;
        this.state = {
            userInfo: userInfo,
            baseURI: config.baseURI,
            selectedCategory,
            filters: [],
            filterOptions: [],
            flag: 0,
            filtersLength: 0,
            selectedOptions: [],
            filteredData: [],
            dataNotFound: false,
            isCopied: false
        };

        toast.configure();
    }

    componentDidMount() {
        axios({
            url: `${this.state.baseURI}filters/get-filters`,
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=UTF-8',
                'Authorization': 'Bearer ' + this.state.userInfo.token
            },
            data: {category_name: this.state.selectedCategory}
        }).then(response => {
            if (response.data.status) {
                this.setState({
                    filters: response.data.data,
                    filtersLength: response.data.data.length,
                    dataNotFound: false
                });
                axios({
                    url: `${this.state.baseURI}filters/get-options`,
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Authorization': 'Bearer ' + this.state.userInfo.token
                    },
                    data: {
                        category_name: this.state.selectedCategory,
                        filters: [],
                        next_filter: this.state.filters[0]
                    }
                }).then(res => {
                    if (res.data.status) {
                        this.setState({filterOptions: res.data.data});
                    }
                }).catch(err => {
                    toast.error(err.response.data.message);
                });
            }
        }).catch(error => {
            toast.error(error.response.data.message);
        });
    }

    handleFilters = (selectedOption, selectedOptions) => {
        if (selectedOptions) {
            this.setState(
                {selectedOptions: []},
                () => {
                    this.setState({selectedOptions});
                }
            );
        } else {
            let selectedOptions2 = this.state.selectedOptions;
            selectedOptions2.push({
                heading: this.state.filters[this.flag],
                option: selectedOption.option
            });
            this.setState({selectedOptions: selectedOptions2});
        }

        if (this.state.filtersLength === (this.flag + 1)) {
            let flag = this.flag + 1;
            this.setState({flag});
            this.flag += 1;
            axios({
                url: `${this.state.baseURI}filters/get-data`,
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                    'Authorization': 'Bearer ' + this.state.userInfo.token
                },
                data: {
                    category_name: this.state.selectedCategory,
                    filters: this.state.selectedOptions
                }
            }).then(response => {
                if (response.data.status === true && response.data.data.length > 0) {
                    this.setState({filteredData: response.data.data, filterOptions: [] });
                } else {
                    this.setState({dataNotFound: true});
                }
            }).catch(error => {
                this.setState({filterOptions: [], dataNotFound: true});
            });
        } else {
            let flag = this.flag + 1;
            this.setState({flag});
            this.flag += 1;
            let data;

            if (selectedOptions) {
                data = selectedOptions;
            } else {
                data = this.state.selectedOptions;
            }
            axios({
                url: `${this.state.baseURI}filters/get-options`,
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                    'Authorization': 'Bearer ' + this.state.userInfo.token
                },
                data: {
                    category_name: this.state.selectedCategory,
                    filters: data,
                    next_filter: this.state.filters[this.flag]
                }
            }).then(response => {
                if (response.data.status === true && response.data.data.length > 0) {
                    this.setState({filterOptions: response.data.data });
                    // if (response.data.status === false) {
                    //     // this.setState({dataNotFound: true});
                    // } else {
                    // }
                } else {
                    if (response.data.data.length === 0) {
                        this.setState({dataNotFound: false});
                    }
                    this.handleFilters({heading: '', option: null});
                }
            }).catch(error => {
                this.setState({filters: '', dataNotFound: true});
            });
        }
    }

    copy = (text) => event => {
        if (navigator.clipboard && window.isSecureContext) {
            navigator.clipboard.writeText(text);
            this.setState({isCopied: true});
            setTimeout(() => {
                this.setState({isCopied: false});
            }, 2000);
        } else {
            let textArea = document.createElement("textarea");
            textArea.value = text;
            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();
            return new Promise((res, rej) => {
                document.execCommand('copy') ? res() : rej();
                textArea.remove();
                this.setState({isCopied: true});
                setTimeout(() => {
                    this.setState({isCopied: false});
                }, 2000);
            });
        }
    }

    // For category breadcrumb
    getFiltersByCategory = () => {
        this.setState(
            {filters: [], filterOptions: [], flag: 0, filtersLength: 0, selectedOptions: [], filteredData: []},
            () => this.componentDidMount()
        );
        this.flag = 0;
    }

    // For filter breadcrumb
    getOptionsByCategoryFilter = (selectedOption) => {
        var selectedOptions = [];
        var flag;
        for (let index = 0; index < this.state.selectedOptions.length; index++) {
            const element = this.state.selectedOptions[index];
            selectedOptions.push(element);
            if (element.option === selectedOption.option) {
                flag = index;
                break;
            }
        }
        this.flag = flag;
        this.setState(
            {
                filterOptions: [],
                filteredData: [],
                flag: selectedOptions.length,
                dataNotFound: false
            },
            () => this.handleFilters(selectedOption, selectedOptions)
        );
    }

    render() {
        const { filters, filterOptions, selectedCategory } = this.state;
        return (
            <React.Fragment>
                <Header />
                <div className="breadcrumb text-white mb-0">
                    <Link to='/dashboard' className="text-white">Dashboard</Link> <span>&nbsp; /</span>
                    <Link to='/uploadExcel' className="text-white">&nbsp; Filtered Info</Link>
                </div>

                <section className="fullPageWrap cardsSec">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="pageWrap bg-white shadow-sm rounded-lg">
                                    <div className="process_status_bar">
                                        <ul>
                                            <li onClick={() => this.getFiltersByCategory()}><span>{selectedCategory}</span></li>
                                            <li>
                                                {Object.keys(this.state.selectedOptions).map((key) => (
                                                    this.state.selectedOptions[key].option !== null ? <span key={key} onClick={() => this.getOptionsByCategoryFilter({'heading': '', 'option': this.state.selectedOptions[key].option})}>{this.state.selectedOptions[key].option}</span> : ''
                                                ))}
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="title_area">
                                        <span className={this.state.isCopied?'copiedMsg fadeIn':'copiedMsg fadeOut'}>Copied!</span>
                                        <h2 className="dash_title">{Object.keys(filters).map(filter => { if(parseInt(this.state.flag) === parseInt(filter)) { return filters[this.state.flag] } else { return false; }} )}</h2>
                                        <Link to="/dashboard" className="btn btn-primary"><span>&#10226;</span> Reset</Link>
                                    </div>
                                    <div className="card_btn_wrap">
                                        {Object.keys(filterOptions).map(filter => {
                                            if (filterOptions[filter] !== null && filterOptions[filter] !== '') {
                                                return <Card title={filterOptions[filter]} key={filter} handleEvent={() => this.handleFilters({heading: '', option: filterOptions[filter]})} routeName="#" />;
                                            } else {
                                                return '';
                                            }
                                        })}

                                        {Object.keys(this.state.filteredData).map(key => (
                                            <CopyCard key={key} number={this.state.filteredData[key].inventory_part_number} title={this.state.filteredData[key].trade_description} cost={this.state.filteredData ? parseFloat(this.state.filteredData[key].cost_usd).toFixed(2) : ''} handleEvent={this.copy(this.state.filteredData[key].inventory_part_number)} />
                                        ))}

                                        {!this.state.dataNotFound || <p className="text-center text-secondary w-100">Data Not Found.</p>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <Footer />
            </React.Fragment>
        );
    }
}

export default FilterInfo;