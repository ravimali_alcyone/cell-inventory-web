import React, { Component } from 'react';

class Demo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputFields: [
                {firstName: '', lastName: ''},
            ]
        };
    }

    handleChangeInput = (index, event) => {
        const values =  [...this.state.inputFields];
        values[index][event.target.name] = event.target.value;
        this.setState({inputFields: values});
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log(this.state.inputFields);
    }

    handleAddFields = () => {
        this.setState({inputFields: [...this.state.inputFields, {firstName: '', lastName: ''}]});
    }

    handleRemoveFields = (index) => {
        const values =  [...this.state.inputFields];
        values.splice(index, 1);
        this.setState({inputFields: values});
    }

    render() {
        return (
            <React.Fragment>
                <div className="container mt-5">
                    <div className="row">
                        <div className="col-12">
                            <form action="#" className="form-inline" onSubmit={this.handleSubmit}>
                                {
                                    this.state.inputFields.map((inputField, index) => (
                                        <div key={index} className="w-100 mb-3">
                                            <input type="text" name="firstName" className="form-control" placeholder="First Name" value={inputField.firstName} onChange={event => this.handleChangeInput(index, event)} />
                                            <input type="text" name="lastName" className="form-control ml-3" placeholder="Last Name" value={inputField.lastName} onChange={event => this.handleChangeInput(index, event)} />
                                            <button type="button" className="btn btn-danger ml-3" onClick={() => this.handleRemoveFields(index)}>-</button>
                                            <button type="button" className="btn btn-secondary ml-3" onClick={() => this.handleAddFields()}>+</button>
                                        </div>
                                    ))
                                }
                                <button type="submit" className="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Demo;