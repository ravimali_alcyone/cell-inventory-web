import React, { Component } from 'react';
import Header from './../../components/Header/Header';
import Footer from './../../components/Footer/Footer';
import config from './../../config';
import axios from 'axios';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import './UploadExcel.css';

class UploadExcel extends Component {
    constructor(props) {
        const userInfo = JSON.parse(localStorage.getItem('userInfo'));
        super(props);
        this.state = {
            userInfo: userInfo,
            baseURI: config.baseURI,
            selectedFile: '',
            reqInProgress: false,
        };
        toast.configure();
    }

    onFileChange = (event) => {
        this.setState({ selectedFile: event.target.files[0] });
    };

    onFileUpload = (event) => {
        event.preventDefault();
        const formData = new FormData();

        formData.append(
            "uploadfile",
            this.state.selectedFile,
            this.state.selectedFile.name
        );

        this.setState({ reqInProgress: true });
        axios({
            url: `${this.state.baseURI}admin/uploadfile`,
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                'Authorization': 'Bearer ' + this.state.userInfo.token
            },
            data: formData
        }).then(response => {
            this.setState({ reqInProgress: false });
            if (response.data.status) {
                toast.success(`${response.data.message}`);
                this.setState({selectedFile: ''});
                event.target.reset();
            } else {
                toast.error(`${response.data.message}`);
            }
        }).catch(error => {
            toast.error(error.response.data.message);
            this.setState({ reqInProgress: false });
        });
    };

    render() {
        return (
            <React.Fragment>
                <Header />
                <div className="breadcrumb text-white mb-0">
                    <Link to='/dashboard' className="text-white">Dashboard</Link> <span>&nbsp; /</span>
                    <Link to='/uploadExcel' className="text-white">&nbsp; Upload Excel</Link>
                </div>

                <section className="fullPageWrap content_wrapper upload_excel">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <form encType="multipart/form-data" onSubmit={this.onFileUpload}>
                                    <div className="input-group mt-5 w-50 mx-auto pb-4">
                                        <div className="custom-file">
                                            <input type="file" className="custom-file-input" id="inputGroupFile02" onChange={this.onFileChange} required={true} accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
                                            <label className="custom-file-label">{this.state.selectedFile.name || 'Choose file'}</label>
                                        </div>
                                        <div className="input-group-append">
                                            <button type="submit" className="btn_ani btn btn-primary btn-small" disabled={this.state.reqInProgress}>{!this.state.reqInProgress || <span className="spinner-border spinner-border-sm mr-1"></span> } Upload</button>
                                        </div>
                                        <small className="info_txt position-absolute">Please upload excel (.xlsx) file only.</small>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>

                <Footer />
            </React.Fragment>
        );
    }
}

export default UploadExcel;