import React, { Component } from 'react';
import { Link } from "react-router-dom";
import config from './../../config';
import axios from 'axios';
import { toast } from 'react-toastify';
import { Formik } from 'formik';
import 'react-toastify/dist/ReactToastify.css';
import './ForgotPassword.css';

class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            baseURI: config.baseURI,
            reqCompleted: false,
            email: ''
        };
        toast.configure();
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = (value) => {
        this.setState({ reqCompleted: true });
        let data = {
            email: value.email
        };

        axios({
            url: `${this.state.baseURI}users/forgot-password`,
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=UTF-8'
            },
            data: data
        }).then(response => {
            this.setState({ reqCompleted: false });
            if (response.data.status) {
                toast.success(response.data.message);
                localStorage.setItem('userEmail', value.email);
                this.props.history.push('/resetPwd');
            } else {
                toast.error(response.data.message);
            }
        }).catch(error => {
            toast.error(error.response.data.message);
            this.setState({ reqCompleted: false });
        });
    }

    render() {
        return (
            <React.Fragment>
                <section className="page_wrap forgot_pwd">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="cp_login_box border rounded-lg p-4 bg-light mx-auto">
                                    <div className="title_box text-center">
                                        <h1 className="primary_title">Password Recovery Email</h1>
                                        <p>Enter the email address associated to the account for which you wish to recover the password</p>
                                    </div>

                                    <Formik
                                        initialValues={{ email: '' }}
                                        validate={values => {
                                            const errors = {};
                                            if (!values.email) {
                                                errors.email = 'Email is required.';
                                            } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
                                                errors.email = 'Invalid email address.';
                                            }
                                            return errors;
                                        }}
                                        onSubmit={(values, { setSubmitting }) => {
                                            this.handleSubmit(values);
                                            setTimeout(() => {
                                                setSubmitting(false);
                                            }, 400);
                                        }}
                                    >
                                        {({
                                            values,
                                            errors,
                                            touched,
                                            handleChange,
                                            handleBlur,
                                            handleSubmit,
                                            isSubmitting
                                        }) => (
                                            <form className="mt-5" onSubmit={handleSubmit}>
                                                <div className="form-group text-left">
                                                    <label>User Email Address<sup className="text-danger">*</sup></label>
                                                    <input type="email" name="email" value={values.email} onChange={handleChange} onBlur={handleBlur} className="form-control" placeholder="User Email" />
                                                    <small className="text-danger">{errors.email && touched.email && errors.email}</small>
                                                </div>
                                                <div className="from_btm_btns_grp mb-0 d-flex justify-content-between">
                                                    <button type="button" className="btn btn-primary btn_ani" id="btnBackCpLogin"><Link to="/" className="text-white text-decoration-none">Back to User Login</Link></button>
                                                    <button type="submit" className={"btn btn-primary btn_ani w-25 float-right" + (this.state.reqCompleted ? ' cursor_no_drop' : '')} id="btnPwdRecovery" disabled={isSubmitting}>{!this.state.reqCompleted || <span className="spinner-border spinner-border-sm mr-1"></span> } Submit</button>
                                                </div>
                                            </form>
                                        )}
                                    </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default ForgotPassword;