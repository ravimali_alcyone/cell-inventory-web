import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PartialRoute = ({children, ...rest}) => {
    let isAuth = false;
    // let data = { ...rest };
    // console.log({...rest});
    // console.log(data.loggedIn);
    if (isAuth) {
        return (<Route {...rest} render={ () => isAuth ? (<Redirect to="/" />) : (children)} />)
    } else {
        return (<Route {...rest} render={ () => isAuth ? (children) : (<Redirect to="/" />)} />)
    }
}

export default PartialRoute;